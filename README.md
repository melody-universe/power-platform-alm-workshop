In order to export a solution, please create a file in at the root of your repository called `.env.json` with the following contents:

```json
{
  "url": "<URL to your development environment>",
  "clientId": "<Client ID>",
  "clientSecret": "<Client Secret>",
  "tenantId": "<Tenant ID>"
}
```
