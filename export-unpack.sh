#!/bin/bash

config=$(cat .env.json)
url=$(echo $config | jq -r '.url')
clientId=$(echo $config | jq -r '.clientId')
clientSecret=$(echo $config | jq -r '.clientSecret')
tenantId=$(echo $config | jq -r '.tenantId')

pac auth create \
  --name GitLabCoreDev \
  --url $url \
  --applicationId $clientId \
  --clientSecret $clientSecret \
  --tenant $tenantId

rm Core.zip Core_managed.zip

pac solution export \
  --name Core \
  --path ./Core.zip \
  --async
pac solution export \
  --name Core \
  --path ./Core_managed.zip \
  --managed \
  --async

pac solution unpack \
  --zipfile ./Core.zip \
  --folder Core \
  --packagetype Both

rm Core.zip Core_managed.zip
pac auth delete --name GitLabCoreDev